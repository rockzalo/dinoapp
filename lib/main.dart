import 'dart:async';

import 'package:flutter/material.dart';
import 'package:dinoreferences/services.dart';
import 'package:dinoreferences/screens/dino_show_screen.dart';
import 'package:dinoreferences/models/dinosaur.dart';
import 'package:dinoreferences/components/menu.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final Future<List<dynamic>> dinosaurs;

  MyApp({Key key, this.dinosaurs}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dino Reference',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: DinoIndexPage(title: 'Dinos para Pablito'),
    );
  }
}

class DinoIndexPage extends StatefulWidget {
  DinoIndexPage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<DinoIndexPage> {
  Future<List<dynamic>> dinosaur;

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      drawer: Menu(),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              child: Text('my dinos',
                style: TextStyle(fontFamily: 'Lost World', fontSize: 40),
              ),
              padding: EdgeInsets.all(20),
            ),
            Expanded(
              child: FutureBuilder(
                future: ApiService.fetchDinosaurs(),
                builder: (context, snapshot) {
                  final dinosaurs = snapshot.data;
                  if (snapshot.connectionState == ConnectionState.done) {
                    return Container(
                      child: GridView.count(
                        crossAxisCount: 2,
                        children:
                        List<Widget>.generate(dinosaurs.length, (index) {
                          return GridTile(
                            child: GestureDetector(
                              onTap: () =>
                              {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          DinoShowScreen(
                                              dinosaur: Dinosaur.fromJson(
                                                  dinosaurs[index]))),
                                )
                              },
                              child: Card(
                                clipBehavior: Clip.antiAlias,
                                elevation: 2,
                                child: Column(
                                  children: <Widget>[
                                    Image.network(
                                      dinosaurs[index]['attributes']
                                      ['image_url'],
                                      height: 120,
                                      fit: BoxFit.contain,
                                    ),
                                    Text(
                                        dinosaurs[index]['attributes']['name']),
                                    Text(
                                        dinosaurs[index]['attributes']
                                        ['name_meaning'],
                                        style: TextStyle(
                                            fontStyle: FontStyle.italic))
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
                      ),
                    );
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      backgroundColor: Colors.amber,
    );
  }
}
