class Dinosaur {
  String id;
  String type;
  Attributes attributes;

  Dinosaur({
    this.id,
    this.type,
    this.attributes,
  });

  factory Dinosaur.fromJson(Map<String, dynamic> json) => Dinosaur(
        id: json["id"],
        type: json["type"],
        attributes: Attributes.fromJson(json["attributes"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "type": type,
        "attributes": attributes.toJson(),
      };
}

class Attributes {
  String name;
  dynamic description;
  String namePronunciation;
  String nameMeaning;
  String imageUrl;
  String length;
  String weight;
  String diet;
  String whenItLived;
  String foundIn;
  String type;

  Attributes({
    this.name,
    this.description,
    this.namePronunciation,
    this.nameMeaning,
    this.imageUrl,
    this.length,
    this.weight,
    this.diet,
    this.whenItLived,
    this.foundIn,
    this.type,
  });

  factory Attributes.fromJson(Map<String, dynamic> json) => Attributes(
        name: json["name"],
        description: json["description"] ?? 'No description',
        namePronunciation: json["name_pronunciation"],
        nameMeaning: json["name_meaning"],
        imageUrl: json["image_url"],
        length: json["length"] ?? 'No length',
        weight: json["weight"] ?? 'No weight',
        diet: json["diet"],
        whenItLived: json["when_it_lived"],
        foundIn: json["found_in"],
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "description": description,
        "name_pronunciation": namePronunciation,
        "name_meaning": nameMeaning,
        "image_url": imageUrl,
        "length": length,
        "weight": weight,
        "diet": diet,
        "when_it_lived": whenItLived,
        "found_in": foundIn,
        "type": type,
      };
}
