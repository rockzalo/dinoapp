import 'package:flutter/material.dart';
import 'package:dinoreferences/screens/dino_collection_screen.dart';

class Menu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var types = [
      "Armoured Dinosaur",
      "Ceratopsian",
      "Euornithopod",
      "Large Theropod",
      "Sauropod",
      "Small Theropod"
    ];
    var diet = [
      "Carnivorous",
      "Herbivorous",
      "Herbivorous/Omnivorous",
      "Omnivorous",
      "Unknown"
    ];

    return Drawer(
      child: SafeArea(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            ExpansionTile(
              leading: Icon(Icons.filter_list),
              title: Text("Search by Type"),
              children: <Widget>[
                for (var item in types)
                  ListTile(
                    title: Text(item),
                    onTap: () => {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DinoCollectionScreen(
                                filterQuery: 'dinosaur_type_name_cont',
                                filterTitle: item),
                          ))
                    },
                  )
              ],
            ),
            ExpansionTile(
              leading: Icon(Icons.filter_list),
              title: Text("Search by Diet"),
              children: <Widget>[
                for (var item in diet)
                  ListTile(
                    title: Text(item),
                    onTap: () => {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DinoCollectionScreen(
                                filterQuery: 'diet_cont', filterTitle: item),
                          ))
                    },
                  )
              ],
            ),
            ListTile(
              title: Text('Settings'),
              leading: Icon(Icons.settings),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
          ],
        ),
      ),
    );
  }
}
