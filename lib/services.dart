import 'dart:convert';

import 'package:http/http.dart' as http;

class URLS {
  static const String BASE_URL = 'simpledinoapi.herokuapp.com';
}

class ApiService {
  static Future<List<dynamic>> fetchDinosaurs(
      { String filterQuery, String filterTitle }) async {
    var response;
    if (filterQuery is String && filterTitle is String) {
      Map<String, String> query = {
        'q[$filterQuery]': filterTitle
      };
      var uri = Uri.https(URLS.BASE_URL, '/dinosaurs', query);
      response = await http.get(uri);
    } else {
      var uri = Uri.https(URLS.BASE_URL, '/dinosaurs');
      response = await http.get(uri);
    }

    if (response.statusCode == 200) {
      return json.decode(response.body)['data'];
    } else {
      return null;
    }
  }
}
