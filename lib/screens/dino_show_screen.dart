import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:dinoreferences/models/dinosaur.dart';

class DinoShowScreen extends StatelessWidget {
  final Dinosaur dinosaur;

  DinoShowScreen({Key key, @required this.dinosaur}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(dinosaur.attributes.name),
      ),
      body: Column(
        children: <Widget>[
          Image.network(dinosaur.attributes.imageUrl),
          Row(
            children: <Widget>[
              Text('Name Meaning: ',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              Text(dinosaur.attributes.nameMeaning),
            ],
          ),
          Row(
            children: <Widget>[
              Text('Name Pronunciation: ',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              Text(dinosaur.attributes.namePronunciation),
            ],
          ),
          Row(
            children: <Widget>[
              Text('Type: ', style: TextStyle(fontWeight: FontWeight.bold)),
              Text(dinosaur.attributes.type),
            ],
          ),
          Row(
            children: <Widget>[
              Text('Length: ', style: TextStyle(fontWeight: FontWeight.bold)),
              Text(dinosaur.attributes.length)
            ],
          ),
          Row(
            children: <Widget>[
              Text('Weight: ', style: TextStyle(fontWeight: FontWeight.bold)),
              Text(dinosaur.attributes.weight)
            ],
          ),
          Row(
            children: <Widget>[
              Text('Diet: ', style: TextStyle(fontWeight: FontWeight.bold)),
              Text(dinosaur.attributes.diet),
            ],
          ),
          Container(
            child: Html(data: dinosaur.attributes.description),
          )
        ],
      ),
    );
  }
}
