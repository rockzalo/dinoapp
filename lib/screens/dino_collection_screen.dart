import 'package:flutter/material.dart';
import 'package:dinoreferences/services.dart';
import 'package:dinoreferences/screens/dino_show_screen.dart';
import 'package:dinoreferences/models/dinosaur.dart';

class DinoCollectionScreen extends StatelessWidget {
  String filterQuery;
  String filterTitle;

  DinoCollectionScreen(
      {Key key, @required this.filterQuery, @required this.filterTitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DinoCollection(filterTitle: filterTitle, filterQuery: filterQuery);
  }
}

class DinoCollection extends StatefulWidget {
  final String filterQuery;
  final String filterTitle;

  DinoCollection({Key key, this.filterQuery, this.filterTitle})
      : super(key: key);

  @override
  _DinoCollectionState createState() => _DinoCollectionState();
}

class _DinoCollectionState extends State<DinoCollection> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.filterTitle),
      ),
      body: FutureBuilder(
        future: ApiService.fetchDinosaurs(
            filterQuery: widget.filterQuery, filterTitle: widget.filterTitle),
        builder: (context, snapshot) {
          final dinosaurs = snapshot.data;
          if (snapshot.connectionState == ConnectionState.done) {
            return Container(
              child: GridView.count(
                crossAxisCount: 2,
                children: List<Widget>.generate(dinosaurs.length, (index) {
                  return GridTile(
                    child: GestureDetector(
                      onTap: () => {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DinoShowScreen(
                                  dinosaur:
                                      Dinosaur.fromJson(dinosaurs[index]))),
                        )
                      },
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        elevation: 2,
                        child: Column(
                          children: <Widget>[
                            Image.network(
                              dinosaurs[index]['attributes']['image_url'],
                              height: 120,
                              fit: BoxFit.contain,
                            ),
                            Text(dinosaurs[index]['attributes']['name']),
                            Text(dinosaurs[index]['attributes']['name_meaning'],
                                style: TextStyle(fontStyle: FontStyle.italic))
                          ],
                        ),
                      ),
                    ),
                  );
                }),
              ),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
